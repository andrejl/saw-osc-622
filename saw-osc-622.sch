EESchema Schematic File Version 4
LIBS:saw-osc-622-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L osc:VS-500 U1
U 1 1 6053A3CC
P 4700 4050
F 0 "U1" H 4700 4515 50  0000 C CNN
F 1 "VS-500" H 4700 4424 50  0000 C CNN
F 2 "saw-osc-622:VS-500" H 4300 4350 50  0001 C CNN
F 3 "" H 4300 4350 50  0001 C CNN
	1    4700 4050
	1    0    0    -1  
$EndComp
$Comp
L osc:WBC1-1LB TR1
U 1 1 6053A793
P 6600 4200
F 0 "TR1" H 6600 3775 50  0000 C CNN
F 1 "WBC1-1LB" H 6600 3866 50  0000 C CNN
F 2 "saw-osc-622:WBC1-1LB" H 5500 5100 50  0001 C CNN
F 3 "" H 6600 4200 50  0001 C CNN
	1    6600 4200
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 6053B1AF
P 5200 4450
F 0 "R2" H 5130 4404 50  0000 R CNN
F 1 "470" H 5130 4495 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 5130 4450 50  0001 C CNN
F 3 "~" H 5200 4450 50  0001 C CNN
	1    5200 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 6053B879
P 5550 4450
F 0 "R3" H 5480 4404 50  0000 R CNN
F 1 "470" H 5480 4495 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 5480 4450 50  0001 C CNN
F 3 "~" H 5550 4450 50  0001 C CNN
	1    5550 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:C C3
U 1 1 6053C0E6
P 5200 2300
F 0 "C3" V 4948 2300 50  0000 C CNN
F 1 "100n" V 5039 2300 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 5238 2150 50  0001 C CNN
F 3 "~" H 5200 2300 50  0001 C CNN
	1    5200 2300
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 6053CCDB
P 5200 2800
F 0 "C4" V 4948 2800 50  0000 C CNN
F 1 "10n" V 5039 2800 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 5238 2650 50  0001 C CNN
F 3 "~" H 5200 2800 50  0001 C CNN
	1    5200 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 4050 5550 4050
Wire Wire Line
	5850 4400 5850 4200
Wire Wire Line
	5850 4200 5200 4200
Wire Wire Line
	5200 4300 5200 4200
Connection ~ 5200 4200
Wire Wire Line
	5200 4200 5000 4200
Wire Wire Line
	5550 4300 5550 4050
$Comp
L Transistor_BJT:BC548 Q1
U 1 1 6054104C
P 3400 2150
F 0 "Q1" V 3728 2150 50  0000 C CNN
F 1 "BC548" V 3637 2150 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3600 2075 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC550-D.pdf" H 3400 2150 50  0001 L CNN
	1    3400 2150
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 60541A4F
P 2600 3250
F 0 "D2" V 2639 3132 50  0000 R CNN
F 1 "modra" V 2548 3132 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 2600 3250 50  0001 C CNN
F 3 "~" H 2600 3250 50  0001 C CNN
	1    2600 3250
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D1
U 1 1 6054207D
P 2600 2800
F 0 "D1" V 2639 2682 50  0000 R CNN
F 1 "modra" V 2548 2682 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 2600 2800 50  0001 C CNN
F 3 "~" H 2600 2800 50  0001 C CNN
	1    2600 2800
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C1
U 1 1 60542447
P 3400 2900
F 0 "C1" H 3518 2946 50  0000 L CNN
F 1 "10u" H 3518 2855 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 3438 2750 50  0001 C CNN
F 3 "~" H 3400 2900 50  0001 C CNN
	1    3400 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 6054289D
P 4150 2700
F 0 "C2" H 4268 2746 50  0000 L CNN
F 1 "10u" H 4268 2655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 4188 2550 50  0001 C CNN
F 3 "~" H 4150 2700 50  0001 C CNN
	1    4150 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6053BBCF
P 2600 2350
F 0 "R1" H 2450 2250 50  0000 C CNN
F 1 "1k2" H 2484 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 2530 2350 50  0001 C CNN
F 3 "~" H 2600 2350 50  0001 C CNN
	1    2600 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	2600 2200 2600 2050
Wire Wire Line
	2600 2050 3200 2050
Wire Wire Line
	3600 2050 4150 2050
Wire Wire Line
	5050 2050 5050 2300
Wire Wire Line
	5050 2800 5050 2300
Connection ~ 5050 2300
Wire Wire Line
	5000 3900 5050 3900
Connection ~ 5050 2800
Wire Wire Line
	4150 2550 4150 2050
Wire Wire Line
	3400 2750 3400 2550
Wire Wire Line
	2600 2650 2600 2550
Wire Wire Line
	2600 2550 3400 2550
Connection ~ 2600 2550
Wire Wire Line
	2600 2550 2600 2500
Connection ~ 3400 2550
Wire Wire Line
	3400 2550 3400 2350
Wire Wire Line
	2600 2950 2600 3100
Wire Wire Line
	4400 4050 4150 4050
Wire Wire Line
	4150 4050 4150 3300
$Comp
L power:GND #PWR01
U 1 1 60562B30
P 2600 3400
F 0 "#PWR01" H 2600 3150 50  0001 C CNN
F 1 "GND" H 2605 3227 50  0000 C CNN
F 2 "" H 2600 3400 50  0001 C CNN
F 3 "" H 2600 3400 50  0001 C CNN
	1    2600 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 605641EF
P 3400 3050
F 0 "#PWR02" H 3400 2800 50  0001 C CNN
F 1 "GND" H 3405 2877 50  0000 C CNN
F 2 "" H 3400 3050 50  0001 C CNN
F 3 "" H 3400 3050 50  0001 C CNN
	1    3400 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 60564435
P 4150 2850
F 0 "#PWR03" H 4150 2600 50  0001 C CNN
F 1 "GND" H 4155 2677 50  0000 C CNN
F 2 "" H 4150 2850 50  0001 C CNN
F 3 "" H 4150 2850 50  0001 C CNN
	1    4150 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 605646DC
P 4250 4200
F 0 "#PWR04" H 4250 3950 50  0001 C CNN
F 1 "GND" H 4255 4027 50  0000 C CNN
F 2 "" H 4250 4200 50  0001 C CNN
F 3 "" H 4250 4200 50  0001 C CNN
	1    4250 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 605649C7
P 5200 4600
F 0 "#PWR05" H 5200 4350 50  0001 C CNN
F 1 "GND" H 5205 4427 50  0000 C CNN
F 2 "" H 5200 4600 50  0001 C CNN
F 3 "" H 5200 4600 50  0001 C CNN
	1    5200 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 60564C4B
P 5550 4600
F 0 "#PWR08" H 5550 4350 50  0001 C CNN
F 1 "GND" H 5555 4427 50  0000 C CNN
F 2 "" H 5550 4600 50  0001 C CNN
F 3 "" H 5550 4600 50  0001 C CNN
	1    5550 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 6056569C
P 6400 4200
F 0 "#PWR09" H 6400 3950 50  0001 C CNN
F 1 "GND" V 6405 4072 50  0000 R CNN
F 2 "" H 6400 4200 50  0001 C CNN
F 3 "" H 6400 4200 50  0001 C CNN
	1    6400 4200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 60567ACB
P 5350 2300
F 0 "#PWR06" H 5350 2050 50  0001 C CNN
F 1 "GND" V 5355 2172 50  0000 R CNN
F 2 "" H 5350 2300 50  0001 C CNN
F 3 "" H 5350 2300 50  0001 C CNN
	1    5350 2300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 6056807B
P 5350 2800
F 0 "#PWR07" H 5350 2550 50  0001 C CNN
F 1 "GND" V 5355 2672 50  0000 R CNN
F 2 "" H 5350 2800 50  0001 C CNN
F 3 "" H 5350 2800 50  0001 C CNN
	1    5350 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 4050 6400 4050
Wire Wire Line
	6400 4050 6400 4000
Connection ~ 5550 4050
Wire Wire Line
	5850 4400 6400 4400
$Comp
L Device:Jumper JP1
U 1 1 6058A007
P 4600 2050
F 0 "JP1" H 4600 2314 50  0000 C CNN
F 1 "Jumper" H 4600 2223 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4600 2050 50  0001 C CNN
F 3 "~" H 4600 2050 50  0001 C CNN
	1    4600 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J2
U 1 1 6058B123
P 7750 4000
F 0 "J2" H 7850 3975 50  0000 L CNN
F 1 "SMA" H 7850 3884 50  0000 L CNN
F 2 "saw-osc-622:SOIC-3" H 7750 4000 50  0001 C CNN
F 3 " ~" H 7750 4000 50  0001 C CNN
	1    7750 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J1
U 1 1 6058CBFE
P 2400 2050
F 0 "J1" H 2318 1825 50  0000 C CNN
F 1 "12V" H 2318 1916 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_5x10mm" H 2400 2050 50  0001 C CNN
F 3 "~" H 2400 2050 50  0001 C CNN
	1    2400 2050
	-1   0    0    1   
$EndComp
Connection ~ 2600 2050
Wire Wire Line
	4150 2050 4300 2050
Connection ~ 4150 2050
Wire Wire Line
	4900 2050 5050 2050
Wire Wire Line
	6800 4000 7550 4000
$Comp
L power:GND #PWR0101
U 1 1 605911F4
P 6800 4400
F 0 "#PWR0101" H 6800 4150 50  0001 C CNN
F 1 "GND" H 6805 4227 50  0000 C CNN
F 2 "" H 6800 4400 50  0001 C CNN
F 3 "" H 6800 4400 50  0001 C CNN
	1    6800 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 605918CF
P 7750 4200
F 0 "#PWR0102" H 7750 3950 50  0001 C CNN
F 1 "GND" H 7755 4027 50  0000 C CNN
F 2 "" H 7750 4200 50  0001 C CNN
F 3 "" H 7750 4200 50  0001 C CNN
	1    7750 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 60595A6E
P 3650 3900
F 0 "J3" H 3568 3675 50  0000 C CNN
F 1 "TUNE" H 3568 3766 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_5x10mm" H 3650 3900 50  0001 C CNN
F 3 "~" H 3650 3900 50  0001 C CNN
	1    3650 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 3900 4400 3900
Wire Wire Line
	5050 2800 5050 3300
$Comp
L Device:R R4
U 1 1 6059CDF9
P 4500 3300
F 0 "R4" V 4293 3300 50  0000 C CNN
F 1 "220" V 4384 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4430 3300 50  0001 C CNN
F 3 "~" H 4500 3300 50  0001 C CNN
	1    4500 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 3300 4350 3300
Wire Wire Line
	4650 3300 5050 3300
Connection ~ 5050 3300
Wire Wire Line
	5050 3300 5050 3900
Wire Wire Line
	4400 4200 4250 4200
$EndSCHEMATC
